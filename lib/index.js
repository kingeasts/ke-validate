'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (rules) {
    var r = {};
    try {
        var _loop = function _loop(k) {
            var t = k.split('|');
            var key = void 0,
                colText = void 0;
            if (t.length) {
                key = t[0];
                colText = t[1];
            } else {
                key = t;
                colText = t;
            }
            r[key] = [];
            var rule = rules[k].split('|');
            rule.forEach(function (rs) {
                var obj = {};
                if (rs.indexOf(':') === -1) {
                    obj[rs] = true;
                } else {
                    var _k = rs.split(':');
                    obj[_k[0]] = _k[1];
                }
                if (rs === 'required') {
                    obj.message = '请填写' + colText;
                } else {
                    obj.message = colText + '格式错误,请重新填写';
                }
                r[key].push(obj);
            });
        };

        for (var k in rules) {
            _loop(k);
        }
        return r;
    } catch (e) {
        console.log('*****************************************');
        console.log('validate 格式错误');
        console.log('*****************************************');
        console.log(e);
    }
};

var validater = {};
validater.install = function (Vue) {
    Vue.prototype.$validater = function (name) {
        var _this = this;

        return new Promise(function (resolve) {
            _this.$refs[name].validate(function (v) {
                if (v) {
                    resolve();
                }
            });
        });
    };
};
var validate = exports.validate = validater;