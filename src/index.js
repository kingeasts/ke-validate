const validater = {}
validater.install = function (Vue) {
	Vue.prototype.$validater = function (name) {
		return new Promise((resolve) => {
			this.$refs[name].validate((v) => {
			   if (v) {
				   resolve()
			   }
			})
		})
	}
}
export const validate = validater

export default function (rules) {
    let r = {}
    try {
        for (let k in rules) {
            let t = k.split('|')
            let key, colText
            if (t.length) {
                key = t[0]
                colText = t[1]
            } else {
                key = t
                colText = t
            }
            r[key] = []
            let rule = rules[k].split('|')
            rule.forEach(rs => {
                let obj = {}
                if (rs.indexOf(':') === -1) {
                    obj[rs] = true
                } else {
                    let _k = rs.split(':')
                    obj[_k[0]] = _k[1]
                }
                if (rs === 'required') {
                    obj.message = '请填写' + colText
                } else {
                    obj.message = colText + '格式错误,请重新填写'
                }
                r[key].push(obj)
            })
        }
        return r
    }catch (e) {
        console.log('*****************************************')
        console.log('validate 格式错误')
        console.log('*****************************************')
        console.log(e)
    }
}